<?php

namespace App\Controller;

use App\Entity\Screenshot;
use App\Repository\ScreenshotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Ulid;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    public function __construct(
        private UrlGeneratorInterface $urlGeneratorInterface,
        private EntityManagerInterface $emi,
        private ScreenshotRepository $screenshotRepository
    ) {
    }
    #[Route('/', name: 'app_home')]
    public function index()
    {

        $imageName = 'gen_2_' . strval(new Ulid()) . ".png";

        Browsershot::url($this->getParameter('LOCAL_URL') . $this->urlGeneratorInterface->generate('app_generated', ['repeatValue' => random_int(1, 20)], UrlGeneratorInterface::ABSOLUTE_PATH))
            ->select('.synthesis-list')
            ->noSandbox()
            ->setScreenshotType('png')
            ->windowSize(1920, 1080)
            ->deviceScaleFactor(2)
            ->hideBackground()
            ->save('./upload/' . $imageName);

        $screenshotList = $this->screenshotRepository->findBy([], ['id' => 'DESC'], 3);

        dump($screenshotList);

        $screenshot = new Screenshot();
        $screenshot->setName($imageName);
        $this->emi->persist($screenshot);
        $this->emi->flush();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'image' => $imageName,
            'screenshot_list' => $screenshotList
        ]);
    }


    #[Route('/generated/{repeatValue}', name: 'app_generated')]
    public function generated(
        ?int $repeatValue = 1
    ): Response {
        return $this->render('generated/index.html.twig', [
            'controller_name' => 'HomeController',
            'repeat_loop' => $repeatValue
        ]);
    }
}
